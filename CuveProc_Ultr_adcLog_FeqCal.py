#-*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
from WaterFlowVelocity import *
import scipy.signal

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False #用来正常显示负号

FeqTopNum = 1
hState = [0]*16
fState = [0]*100
lFeq = []
DifFeq = 390.625


# 简单定义一个FFT函数
def myfft(x,t):
    fft_x = fft(x)                                            #  fft计算
    amp_x = abs(fft_x)/len(x)*2                                 # 纵坐标变换
    label_x = np.linspace(0,int(len(x)/2)-1,int(len(x)/2))    # 生成频率坐标
    amp = amp_x[0:int(len(x)/2)]                              # 选取前半段计算结果即可
    # amp[0] = 0                                              # 可选择是否去除直流量信号
    fs =1/(t[2]-t[1])                                        # 计算采样频率
    fre = label_x/len(x)*fs                                   # 频率坐标变换
    pha = np.unwrap(np.angle(fft_x))                          # 计算相位角并去除2pi跃变
    return amp,fre,pha                                        # 返回幅度和频率


def plotfft(t, x, fre, amp, fileName, offeq=2e3, extxt='', save=False):
    # 绘图
    plt.figure(dpi=100, figsize=(14 ,8))
    plt.subplot(211)
    plt.plot(t, x)
    plt.title(fileName)
    plt.xlabel('Time / s')
    plt.ylabel('Vamplitude / v')

    plt.subplot(212)
    plt.plot(fre[:16], amp[:16])
    plt.title('Amplitute-Frequence-Curve')
    plt.ylabel('Amplitute / a.u.')
    plt.xlabel('Frequence / Hz')
    plt.plot([offeq, offeq], [0, 3500], color='red', linewidth='1.0', linestyle='--')

    if extxt != '':
        plt.text(offeq+100, 1500, extxt)
    if save:
        plt.savefig(fileName + '.png', dpi=160)
        print('save png is ok')
    plt.show()

def dataProc(path, ld):
    feqtxt = ''
    try:
        f = open(path, encoding='utf-8')
    except Exception as e:
        print(e)
        return 'file open err'
    lines = f.readlines()
    for line in lines:
        line = line.replace('\n', '')
        if line.isdigit():
            ld += [int(line)]
        if 'fMax' in line:
            print(line)
            feqtxt = line
    return feqtxt

def SelectMax(amp, fre):
    maxlist = []
    maxfeq = 0
    sortmix = []
    # 最大频率查找
    #maxindex = np.argmax(amp[1:])
    #maxfeq = [fre[maxindex + 1]]

    # 将列表中的数组沿深度方向进行拼接 2维 按第一列排序
    mix = np.dstack((amp[1:], fre[1:]))[0]
    # 按照数组的第一列进行排序
    sortmix = mix[np.argsort(mix[:, 0])[::-1]]


    return sortmix


def multiFeqCal(path, fileName, Fs, OffsetFreq, step=[0, 1, 256]):
    Ts = 1 / Fs  # sample interval
    ld = []
    lv = []

    feqtxt = dataProc(path, ld)
    if feqtxt == 'file open err':
        print('file open err', __name__)
        exit()

    if len(ld) != 1024:
        print('ld len err', __name__)
        exit

    if step[2]*8 >= 1024:
        step[2] = 128
    if step[1] > 8:
        step = 8

    print('========================')
    for i in range(step[0], step[1]):
        try:
            l = ld[step[2] * i:step[2] * (i + 8)]
        except Exception as e:
            print(e)
            break

        feqtxt = 'feq:' + str(OffsetFreq) + 'Hz    data:' + str(step[2] * i) + '~' + str(step[2] * (i + 8))

        t = np.arange(0, len(l) * Ts, Ts)
        amp, fre, pha = myfft(l, t)  # 调用函数
        #plotfft(t, l, fre, amp, fileName, OffsetFreq, feqtxt, False)


        # TODO 2023-6-19
        # fre2 = [i + 1 for i in fre]  # feq add 1e3
        #fre3 = list(map(lambda x: x + DifFeq, fre))  # feq add 1e3
        # ---------------

        # 数字滤波
        # 频率小于32K的进行滤除


        # 最大频率查找
        # maxindex = np.argmax(amp[1:])
        # maxfre = fre[maxindex + 1]
        mix = SelectMax(amp, fre)

        # TODO 2023-6-20
        # 静水判断
        # 当max feq 连续出现xx频率或一组中次数大于N判断为静水
        # f = mix[0][1]

        #lFeq += [f]
        lFeq.append(f)

        # 水速计算
        # 频率选择 top num
        for j in range(FeqTopNum):
            # TODO 2023-6-20
            sf = 32226.5625 # 200e3/1024 * 165
            #if mix[j][1] < sf:
            #    f = mix[j][1] +

            v = FlowCal(mix[j][1], OffsetFreq)
            lv += [v]
            print(feqtxt, ' T:%f, feq%d:%d, v:%f' % (Ts * len(l), j, mix[j][1], v))

            n = mix[j][0] // 50
            hState[int(n)] += 1
            n = mix[j][1] / (200e3/1024) - 150
            if n >= 0 and n < 100:

                # 加权算法 1111
                # fState[int(n)] += 1

                # 加权算法 4321
                #fState[int(n)] += (FeqTopNum - j)
                ''''''
                # 加权算法 1411
                if j == 0:
                    fState[int(n)] += 4
                elif j == 1:
                    fState[int(n)] += 0
                elif j == 2:
                    fState[int(n)] += 0
                elif j == 3:
                    fState[int(n)] += 0

            print('hState:',hState)
            print('fState:', fState)
        print('-------------------------')

    return lv


def multiFeqProc(path, fileName, Fs, OffsetFreq):
    Ts = 1 / Fs  # sample interval
    ld = []
    lf = []

    feqtxt = dataProc(path, ld)
    if feqtxt == 'file open err':
        print('file open err', __name__)
        exit()

    if len(ld) % 1024 != 0:
        print('ld len err', __name__)
        exit


    feqtxt = 'feq:' + str(OffsetFreq) + 'Hz'

    '''
    数据扩展，选择前512点，再进行扩展1024，共计2048点进行FFT计算
    '''
    #l = dataExp(ld, 512, 512)
    l = dataSplitSplic(ld, 0, 512, 1024)

    t = np.arange(0, len(l) * Ts, Ts)
    amp, fre, pha = myfft(l, t)  # 调用函数
    #plotfft(t, l, fre, amp, fileName, OffsetFreq, feqtxt, False)


    '''
    数字滤波，频率小于32K的进行滤除
    '''
    n = findOffsetFeq(fre, OffsetFreq)
    amp = amp[n:]
    fre = fre[n:]

    # 最大频率查找
    mix = SelectMax(amp, fre)


    lf = mix[:FeqTopNum]
    return lf


# 判断频率是否连续 sortmix 从小到大
def feqContin(sortmix, feqStep):  # 三点频率选择
    n = 0
    m = 0
    m = (sortmix[1][1] - sortmix[0][1]) // feqStep
    n = (sortmix[-1][1] - sortmix[0][1]) // feqStep
    z = n / 2
    zfeq = z * feqStep + sortmix[0][1]
    #print(__name__, 0, m, n, z, zfeq)
    return zfeq


'''
1、按频率从小到大排序
2、连续性判断，补点后，选择频率中间值

'''
def feqSelect(lf, feqStep):
    lfeq = []
    for d in lf:
        # 按照频率排序
        # 按照数组的第2列进行排序
        # sortmix = d[np.argsort(d[:, 1])[::-1]] # 从大到小
        sortmix = d[np.argsort(d[:, 1])] # # 从小到大

        # 判断频率连续性
        feq = feqContin(sortmix, feqStep)
        # 判断幅值大小

        #v = FlowCal(feq, 32e3)
        lfeq += [feq]

    return lfeq

def AMPD(data):
    """
    实现AMPD算法
    :param data: 1-D numpy.ndarray
    :return: 波峰所在索引值的列表
    """
    p_data = np.zeros_like(data, dtype=np.int32)
    count = data.shape[0]
    arr_rowsum = []
    for k in range(1, count // 2 + 1):
        row_sum = 0
        for i in range(k, count - k):
            if data[i] > data[i - k] and data[i] > data[i + k]:
                row_sum -= 1
        arr_rowsum.append(row_sum)
    min_index = np.argmin(arr_rowsum)
    max_window_length = min_index
    for k in range(1, max_window_length + 1):
        for i in range(k, count - k):
            if data[i] > data[i - k] and data[i] > data[i + k]:
                p_data[i] += 1
    return np.where(p_data == max_window_length)[0]

def mdl(lState, dt):
    """
    实现AMPD算法
    :param data: 1-D numpy.ndarray
    :return: 波峰所在索引值的列表
    """
    data = lState[:, 1]
    step = dt['step']
    nMax = 0
    nMaxData = 0
    nNop = 0
    for i in range(len(data)-1, -1, -1):
        if nMaxData < data[i]:
            nMaxData = data[i]
            nMax = i
            nNop = 0
        elif nMaxData - 1 <= data[i]: # 统计值相差1,选上一次频率
            if dt["lastFeq"] == lState[:,0][i]:
                nMaxData = data[i]
                nMax = i
                nNop = 0
        else:
            nNop += 1
            if nNop >= step and nMaxData >= dt['threshold']:
                break
    dt["lastFeq"] = dt["lastFeq"] * 0.9 + lState[:, 0][nMax] * 0.1
    return [nMax]

'''
    1、top4频率点统计，仅统计过去的24个点
    2、基于统计中间值，+1K，作为右门限值
    3、选择4点中频率最大的
'''
def feqSelectOneMax(l, dt):
    lState = []

    # 统计前24个点的频率
    # lState = np.unique(lf[:24, 1])
    # 取前24个
    # 取2维的所有
    # 取3维的第2个

    for i in np.unique(l):
        # print(i, np.sum(l == i))
        lState += [[i, np.sum(l == i)]]
    lState = np.array(lState)

    try:
        # 查找多点数据峰值
        #peaks = scipy.signal.find_peaks_cwt(lState[:,1], 3)
        #peaks = AMPD(lState[:, 1])
        peaks = mdl(lState, dt)
        print(peaks, lState[:,1][peaks], lState[:,0][peaks])
        feq = lState[:,0][peaks[-1]]
    except Exception as e:
        print(e)
        feq = 32e3

    return feq

def feqSelectMax(lf, dt, node=24):
    lfeq = []
    for i in range(node-1):
        l = lf[0:i, :, 1]
        feq = feqSelectOneMax(l, dt)
        lfeq += [feq]

    for i in range(len(lf)-node):
        l = lf[i:i+node, :, 1]
        feq = feqSelectOneMax(l, dt)
        lfeq += [feq]

    return lfeq

if __name__ == '__main__':
    import os
    Fs = 200e3
    OffsetFreq = 32e3
    feqStep = Fs / 1024
    #path = 'data221228-声研所2-180Hz-9.txt'
    rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-6'
    dirpath = u'\\2023-6-5-8k-v0\\'
    fileName = u'2023-6-5-8k-v03.txt'
    #print(fileName)
    path = rootpath + dirpath + fileName

    lf = multiFeqProc(path, fileName, Fs, OffsetFreq)
    for f in lf:
        print(f)
