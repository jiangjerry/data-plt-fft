import numpy as np

'''
Δf:频率偏移
V:水流速度
C:超声波水速
Λ:基础频偏
Δf = 2V * f / C + Λ
'''

vHz = {'0hz': 0, '1hz': 0.07, '2hz': 0.118, '3hz': 0.19, '4hz': 0.3,
       '5hz': 0.38, '6hz': 0.44, '7hz': 0.5, '8hz': 0.56, '9hz': 0.118,
       '10hz': 0.118, '11hz': 0.118, '12hz': 0.118, '13hz': 0.118, '14hz': 0.118,
       '15hz': 0.118}


def FlowCal(freq, offset_freq=32e3):
    c = 1500
    div = 2
    # SampFeq = 8e6 / div / (12.5+2.5+5)
    baseFeq = 2e6

    v = (freq - offset_freq) * c / (2 * baseFeq)
    return v

def ListFlowCal(lf):
    lv = []
    for feq in lf:
        v = FlowCal(feq, 32e3)
        lv += [v]
    return lv

def FeqCal(v, offset_freq=32e3):
    c = 1500
    baseFeq = 2e6

    f = 2 * v * baseFeq / c + offset_freq
    return f

def PumpFeqCal(fileName):
    i = fileName.find('hz')
    if i < 0:
        i = fileName.find('Hz')
    hz = fileName[i-2:i]
    hz = hz.replace('-', '')
    ihz = int(hz)
    v = 0.065*ihz

    f = FeqCal(v)

    return f

def PumpVCal(fileName):
    i = fileName.find('hz')
    if i < 0:
        i = fileName.find('Hz')
    hz = fileName[i-2:i]
    hz = hz.replace('-', '')
    ihz = int(hz)
    v = 0.065*ihz

    return v


def dataExp(d, l=1024, e=4096):
    if len(d) < l:
        l = len(d)
    else:
        d = d[:l]
    avg = np.average(d)
    d = np.array(d)
    e = np.full(e, avg)
    d = np.hstack((d,e))
    return d.tolist()


def dataSplitSplic(d, s, m, n):
    l = len(d)
    sd = []
    c = 0
    while c * n + m <= l:
        sd += d[c * n + s: c * n + m]
        c += 1
    return sd


def findOffsetFeq(fre, OffsetFreq):
    for i in range(len(fre)):
        if fre[i] > OffsetFreq:
            return i - 1
    return 0


if __name__ == '__main__':
    '''
    for f in range(2000, 3000, 20):
        v = FlowCal(f)
        print('f=%f, v=%f' %(f, v))
    '''
    fileName = '2023-6-15-32k-10hz002'
    v = PumpFeqCal(fileName)
    print(v)



