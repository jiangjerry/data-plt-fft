#-*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False #用来正常显示负号

# 简单定义一个FFT函数
def myfft(x,t):
    fft_x = fft(x)                                            #  fft计算
    amp_x = abs(fft_x)/len(x)*2                                 # 纵坐标变换
    label_x = np.linspace(0,int(len(x)/2)-1,int(len(x)/2))    # 生成频率坐标
    amp = amp_x[0:int(len(x)/2)]                              # 选取前半段计算结果即可
    # amp[0] = 0                                              # 可选择是否去除直流量信号
    fs =1/( t[2]-t[1])                                        # 计算采样频率
    fre = label_x/len(x)*fs                                   # 频率坐标变换
    pha = np.unwrap(np.angle(fft_x))                          # 计算相位角并去除2pi跃变
    return amp,fre,pha                                        # 返回幅度和频率


def plotfft(t, x, fre, amp, pathList, extxt='', save=False):
    # 绘图
    plt.figure(dpi=100, figsize=(14 ,8))
    plt.subplot(211)
    plt.plot(t, x)
    plt.title(pathList[2])
    plt.xlabel('Time / s')
    plt.ylabel('Vamplitude / v')

    plt.subplot(212)
    plt.plot(fre[:16], amp[:16])
    plt.title('Amplitute-Frequence-Curve')
    plt.ylabel('Amplitute / a.u.')
    plt.xlabel('Frequence / Hz')
    plt.plot([2000, 2000], [0, 3500], color='red', linewidth='1.0', linestyle='--')

    if extxt != '':
        plt.text(2100, 1500, extxt)
    if save:
        plt.savefig(pathList[0] + pathList[1] + pathList[2][:-4] + '.png', dpi=160)
        print('save png is ok')
    plt.show()

def dataProc(path, ld):
    feqtxt = ''
    f = open(path, encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        line = line.replace('\n', '')
        if line.isdigit():
            ld += [int(line)]
        if 'fMax' in line:
            print(line)
            feqtxt = line
    return feqtxt


if __name__ == '__main__':
    import os
    Fs = 50e3
    Ts = 1 / Fs          # sample interval
    ld = []
    feqtxt = ''
    #path = 'data221228-声研所2-180Hz-9.txt'
    rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-6'
    dirpath = u'\\2023-6-5-2k-10hz-cu\\'
    fileName = u'2023-6-5-2k-10hz-cu12.txt'
    #print(fileName)
    path = rootpath + dirpath + fileName
    pathList = [rootpath, dirpath, fileName]

    feqtxt = dataProc(path, ld)

    for i in range(8):
        l = ld[128*i:128*(i+1)]

        feqtxt = 'feq:2000Hz    data:' + str(128*i) + '~' + str(128*(i+1))

        t = np.arange(0, len(l)*Ts, Ts)
        amp, fre, pha = myfft(l, t)  # 调用函数
        plotfft(t, l, fre, amp, pathList, feqtxt, False)

