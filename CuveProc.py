#-*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False #用来正常显示负号

# 简单定义一个FFT函数
def myfft(x,t):
    fft_x = fft(x)                                            #  fft计算
    amp_x = abs(fft_x)/len(x)*2                                 # 纵坐标变换
    label_x = np.linspace(0,int(len(x)/2)-1,int(len(x)/2))    # 生成频率坐标
    amp = amp_x[0:int(len(x)/2)]                              # 选取前半段计算结果即可
    # amp[0] = 0                                              # 可选择是否去除直流量信号
    fs =1/( t[2]-t[1])                                        # 计算采样频率
    fre = label_x/len(x)*fs                                   # 频率坐标变换
    pha = np.unwrap(np.angle(fft_x))                          # 计算相位角并去除2pi跃变
    return amp,fre,pha                                        # 返回幅度和频率


def plotfft(t, x, fre, amp, title='title'):
    # 绘图
    plt.subplot(211)
    plt.plot(t, x)
    plt.title(title)
    plt.xlabel('Time / s')
    plt.ylabel('Vamplitude / v')

    plt.subplot(212)
    plt.plot(fre, amp)
    plt.title('Amplitute-Frequence-Curve')
    plt.ylabel('Amplitute / a.u.')
    plt.xlabel('Frequence / Hz')
    plt.show()


if __name__ == '__main__':
    import os
    Fs = 4e3
    Ts = 1 / Fs          # sample interval
    ld = []
    #path = 'data221228-声研所2-180Hz-9.txt'
    path = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\4k-40k-step4-15ms-5ms.txt'
    fileName = os.path.basename(path)
    print(fileName)


    f = open(path, encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        ld += [int(line)*2.4/4096/4096]

    t = np.arange(0, len(ld)*Ts, Ts)
    amp, fre, pha = myfft(ld, t)  # 调用函数
    plotfft(t, ld, fre, amp, fileName)

