#-*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
import os
from CuveProc_Ultr_adcLog_FeqCal import *
from random import random

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False #用来正常显示负号

# 简单定义一个FFT函数
def myfft(x,t):
    fft_x = fft(x)                                            #  fft计算
    amp_x = abs(fft_x)/len(x)*2                                 # 纵坐标变换
    label_x = np.linspace(0,int(len(x)/2)-1,int(len(x)/2))    # 生成频率坐标
    amp = amp_x[0:int(len(x)/2)]                              # 选取前半段计算结果即可
    # amp[0] = 0                                              # 可选择是否去除直流量信号
    fs =1/( t[2]-t[1])                                        # 计算采样频率
    fre = label_x/len(x)*fs                                   # 频率坐标变换
    pha = np.unwrap(np.angle(fft_x))                          # 计算相位角并去除2pi跃变
    return amp,fre,pha                                        # 返回幅度和频率


def plotfft(t, x, fre, amp, pathList, extxt='', save=False, feqPos=32000):
    # draw sort freq point
    mix = SelectMax(amp, fre)
    Tmin = mix.T
    px = Tmin[1][:FeqTopNum]
    py = Tmin[0][:FeqTopNum]
    end = int(px.max()//fre[1]) + 3
    start = int(px.min()//fre[1] - 3)

    # 绘图
    plt.figure(dpi=100, figsize=(14 ,8))
    plt.subplot(211)
    plt.plot(t[:], x[:])
    plt.title(pathList[1])
    plt.xlabel('Time / s')
    plt.ylabel('Vamplitude / v')

    plt.subplot(212)
    plt.plot(fre[start:end], amp[start:end])
    #plt.plot(fre[64:196], amp[64:196])
    #plt.plot(fre, amp)
    #plt.plot(feqPos)

    # draw 32e3
    plt.plot([feqPos,feqPos],[0,80], color='g', linewidth='1.0', linestyle='--')

    # draw v freq
    plt.plot([32e3, 32e3], [0, 80], color='r', linewidth='1.0', linestyle='--')

    # 数字滤波
    # 频率小于32K的进行滤除
    # feqStep = Fs / 1024
    # 230 # 32K
    #amp = amp[230:]
    #fre = fre[230:]

    #plt.plot(px, py, 'o')
    z = list(range(FeqTopNum))
    plt.scatter(px, py, c=z, cmap='Greens_r')
    #print(px, py)


    plt.title('Amplitute-Frequence-Curve')
    plt.ylabel('Amplitute / a.u.')
    plt.xlabel('Frequence / Hz')

    if extxt != '':
        plt.text(35000, 50, extxt)
    if save:
        pltpath = pathList[0] + '\\' + pathList[1][:-4] + '+512x4.png'
        plt.savefig(pltpath, dpi=160)
        print('save png is ok')
    #plt.show()


def plotFFTDouble(t, x, fre, amp, t2, x2, fre2, amp2, pathList, extxt='', save=False, feqPos=32000):
    # draw sort freq point
    mix = SelectMax(amp, fre)
    Tmin = mix.T
    px = Tmin[1][:FeqTopNum]
    py = Tmin[0][:FeqTopNum]
    end = int(px.max()//fre[1]) + 3
    start = int(px.min()//fre[1] - 3)

    # 绘图
    plt.figure(dpi=100, figsize=(14 ,8))
    plt.subplot(411)
    plt.plot(t[:], x[:])
    plt.title(pathList[1])
    plt.xlabel('Time / s')
    plt.ylabel('Vamplitude / v')

    plt.subplot(412)
    plt.plot(fre[start:end], amp[start:end])
    #plt.plot(fre[64:196], amp[64:196])
    #plt.plot(fre, amp)
    #plt.plot(feqPos)

    # draw 32e3
    plt.plot([feqPos,feqPos],[0,80], color='g', linewidth='1.0', linestyle='--')

    # draw v freq
    plt.plot([32e3, 32e3], [0, 80], color='r', linewidth='1.0', linestyle='--')

    # 数字滤波
    # 频率小于32K的进行滤除
    # feqStep = Fs / 1024
    # 230 # 32K
    #amp = amp[230:]
    #fre = fre[230:]

    #plt.plot(px, py, 'o')
    z = list(range(FeqTopNum))
    plt.scatter(px, py, c=z, cmap='Greens_r')
    #print(px, py)

    mix = SelectMax(amp2, fre2)
    Tmin = mix.T
    px = Tmin[1][:FeqTopNum]
    py = Tmin[0][:FeqTopNum]
    end = int(px.max() // fre2[1]) + 3
    start = int(px.min() // fre2[1] - 3)

    plt.subplot(413)
    plt.plot(t2[:], x2[:])
    plt.xlabel('Time / s')
    plt.ylabel('Vamplitude / v')

    plt.subplot(414)
    plt.plot(fre2[start:end], amp2[start:end])
    # plt.plot(fre[64:196], amp[64:196])
    # plt.plot(fre, amp)
    # plt.plot(feqPos)

    # draw 32e3
    plt.plot([feqPos, feqPos], [0, 80], color='g', linewidth='1.0', linestyle='--')

    # draw v freq
    plt.plot([32e3, 32e3], [0, 80], color='r', linewidth='1.0', linestyle='--')

    # 数字滤波
    # 频率小于32K的进行滤除
    # feqStep = Fs / 1024
    # 230 # 32K
    # amp = amp[230:]
    # fre = fre[230:]

    # plt.plot(px, py, 'o')
    z = list(range(FeqTopNum))
    plt.scatter(px, py, c=z, cmap='Greens_r')


    #plt.title('Amplitute-Frequence-Curve')
    plt.ylabel('Amplitute / a.u.')
    plt.xlabel('Frequence / Hz')

    if extxt != '':
        plt.text(35000, 50, extxt)
    if save:
        pltpath = pathList[0] + '\\' + pathList[1][:-4] + '.png'
        plt.savefig(pltpath, dpi=160)
        print('save png is ok')
    #plt.show()

def traversalSubdirectory(datapath):
    p = os.walk(datapath)
    en = False
    for path, dir_list, file_list in p:
        for file_name in file_list:
            #f = os.path.join(path, file_name)
            if '.txt' in file_name:
                '''
                if '045.txt' in file_name:
                    en = True
                else:
                    en = False
                '''
                en = True

                if en:
                    print(path, file_name)
                    batchPlt(path, file_name)


def batchPlt(dirpath, fileName, baseV=0):
    # Fs = (25e6 / 4) / (2.5 + 12.5 + 7)
    Ts = 1 / Fs  # sample interval
    ld = []
    feqtxt = ''
    # path = 'data221228-声研所2-180Hz-9.txt'
    #rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-6'
    #dirpath = u'\\2023-6-5-2k-v0-cu\\'
    #fileName = u'2023-6-5-2k-v0-cu2.txt'
    # print(fileName)
    path = dirpath + '\\' + fileName
    pathList = [dirpath, fileName]

    f = open(path, encoding='utf-8')
    lines = f.readlines()
    for line in lines:
        line = line.replace('\n', '')
        if line.isdigit():
            try:
                ld += [int(line)]
            except Exception as e:
                print(e)
                return
        if 'fMax' in line:
            print(line)
            feqtxt = line
        elif 'avg =' in line:
            print(line)
            feqtxt += '\n'
            feqtxt += line

    # ld = ld[500:600]
    # ld += [int(line) * 2.4 / 4096 / 4096]

    vFeq = PumpFeqCal(fileName)
    feqPlot = vFeq
    feqtxt += '\nvFeq:'
    feqtxt += str(vFeq)

    #ld = dataExp(ld, 1024, 1024)
    ld1 = dataSplitSplic(ld, 0, 512, 1024)
    ld2 = dataSplitSplic(ld, 0, 256, 1024)

    t1 = np.arange(0, len(ld1) * Ts, Ts)
    amp1, fre1, pha1 = myfft(ld1, t1)  # 调用函数

    t2 = np.arange(0, len(ld2) * Ts, Ts)
    amp2, fre2, pha2 = myfft(ld2, t2)  # 调用函数

    # TODO 2023-6-19
    #fre2 = [i + 1 for i in fre]  # feq add 1e3
    #fre3 = list(map(lambda x: x + 1e3, fre))  # feq add 1e3
    # ---------------
    try:
        # plotfft(t, ld, fre, amp, pathList, feqtxt, True, feqPlot)
        plotFFTDouble(t1, ld1, fre1, amp1, t2, ld2, fre2, amp2, pathList, feqtxt, True, feqPlot)
    except Exception as e:
        print(e)
        return


if __name__ == '__main__':
    rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-30'
    #rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\未来城悠湖测试 2023-6-27'
    dirpath = u'\\2023-6-30-32k-N1-rrs-1Hzeigth-8192\\'
    # dirpath = '\\'

    Fs = (25e6 / 4) / (2.5 + 12.5 + 7)
    FeqTopNum = 8
    #Fs = 200e3

    path = rootpath + dirpath
    traversalSubdirectory(path)
