import os

import numpy as np

from CuveProc_Ultr_adcLog_FeqCal import *
import json


def traversalSubdirectory(datapath):
    lv = []
    lf = []
    p = os.walk(datapath)
    for path, dir_list, file_list in p:
        for file_name in file_list:
            #f = os.path.join(path, file_name)
            if '.txt' in file_name:
                print(path, file_name)

                #path = rootpath + dirpath + fileName
                f = os.path.join(path, file_name)
                #pathList = [rootpath, dirpath, fileName]

                # 速度
                # lv += [multiFeqCal(f, file_name, Fs, OffsetFreq, calStep)]
                # 频率
                lf += [multiFeqProc(f, file_name, Fs, OffsetFreq)]
    return np.array(lf)


def avglvOne(lv):
    avgl = []
    l = []
    x = np.array(lv)

    x1 = x
    for i in range(AvgNum, len(x1) - AvgNum):
        avgl += [np.average(x1[i - AvgNum:i + AvgNum])]

    return avgl

def avglv(lv):
    avgl = []
    l = []
    x = np.array(lv)

    x1 = x[:, 0]
    for i in range(AvgNum, len(x1)-AvgNum):
        avgl += [np.average(x1[i-AvgNum:i+AvgNum])]
    l.append(avgl)

    avgl = []
    x2 = x[:, 1]
    for i in range(AvgNum, len(x2) - AvgNum):
        avgl += [np.average(x2[i - AvgNum:i + AvgNum])]
    l.append(avgl)

    avgl = []
    x3 = x[:, 2]
    for i in range(AvgNum, len(x3) - AvgNum):
        avgl += [np.average(x3[i - AvgNum:i + AvgNum])]
    l.append(avgl)

    avgl = []
    x4 = x[:, 3]
    for i in range(AvgNum, len(x4) - AvgNum):
        avgl += [np.average(x4[i - AvgNum:i + AvgNum])]
    l.append(avgl)

    return l


'''
def sunAvglv(lv):
    avgl = []
    l = []
    x = np.array(lv)

    for k in range(4):
        x1 = x[:, k]
        for i in range(AvgNum, len(x1)-AvgNum):
            xsum = np.sum(x1[i-AvgNum:i+AvgNum])
            xmax = np.max(x1[i-AvgNum:i+AvgNum])
            xmin = np.min(x1[i-AvgNum:i+AvgNum])
            xavg = (xsum - xmax - xmin)/(AvgNum * 2 - 2)
            avgl += [xavg]
        l.append(avgl)
        avgl = []

    return l
'''

def WeightAvglv(lv):
    avgl = []
    l = []
    x = np.array(lv)

    # top1 0.5 + top2 0.5
    x1 = x[:, 0]
    x2 = x[:, 1]

    # top1 0.5 + top2 0.5
    #x1 = x[:, 1]
    #x2 = x[:, 1]

    x12 = [(x + y)/2 for x, y in zip(x1, x2)]
    for i in range(AvgNum, len(x12)-AvgNum):
        # avgl += [np.average(x12[i-AvgNum:i+AvgNum])]

        #   31640.625000
        ndata = np.array(lFeq[i-AvgNum:i+AvgNum])
        navg = np.average(lFeq[i - AvgNum:i + AvgNum])

        n = np.sum((ndata > StopMinFeq) & (ndata < StopMaxFeq))
        nm = np.sum((ndata > StopMaxFeq))
        if n > 8 and nm < 4:
            avgl += [0]
        else:
            if((navg > LowMinFeq) & (navg < LowMaxFeq)):
                avgl += [np.average(x2[i - AvgNum:i + AvgNum])]
            else:
                avgl += [np.average(x12[i - AvgNum:i + AvgNum])]


    l.append(avgl)
    l.append(avgl)
    l.append(avgl)
    l.append(avgl)

    return l

def selMaxAvglv(lv):
    avgl = []
    l = []
    x = np.array(lv)

    x1 = np.max(x, axis=1)
    for i in range(AvgNum, len(x1) - AvgNum):
        xavg = np.average(x1[i-AvgNum:i+AvgNum])
        # xavg = np.max(x1[i-AvgNum:i+AvgNum])
        avgl += [xavg]

    l.append(avgl)
    l.append(avgl)
    l.append(avgl)
    l.append(avgl)

    return l

def selWeightedAvglv(lv):
    avgl = []
    l = []
    x = np.array(lv)

    x1 = np.max(x, axis=1)
    for i in range(AvgNum, len(x1) - AvgNum):
        # xavg = np.min(x1[i-AvgNum:i+AvgNum])
        xavg = np.max(x1[i-AvgNum:i+AvgNum])
        avgl += [xavg]

    l.append(avgl)
    l.append(avgl)
    l.append(avgl)
    l.append(avgl)

    return l


def vplot(lv, lavg=[], title=''):
    t = np.arange(0, len(lv))
    avgt = np.arange(AvgNum, len(lavg[0][0])+AvgNum)
    x = np.array(lv)

    basev = 0
    if title is not '':
        basev = PumpVCal(title)
    lbv = np.full((len(lv),1), basev)

    # 绘图
    plt.figure(dpi=100, figsize=(14, 8))
    plt.subplot(411)
    plt.title(title)
    plt.ylabel('feq1 v / m/s')
    plt.plot(t, x[:, 0])
    plt.plot(avgt, np.array(lavg[0][0]), color='r')
    plt.plot(avgt, np.array(lavg[1][0]), color='g')
    plt.plot(t, lbv, color='m', linewidth='1.0', linestyle='--')

    plt.subplot(412)
    plt.ylabel('feq2 v / m/s')
    plt.plot(t, x[:, 1])
    plt.plot(avgt, np.array(lavg[0][1]), color='r')
    plt.plot(avgt, np.array(lavg[1][1]), color='y')
    plt.plot(t, lbv, color='m', linewidth='1.0', linestyle='--')

    plt.subplot(413)
    plt.ylabel('feq3 v / m/s')
    plt.plot(t, x[:, 2])
    plt.plot(avgt, np.array(lavg[0][2]), color='r')
    plt.plot(avgt, np.array(lavg[1][2]), color='y')
    plt.plot(t, lbv, color='m', linewidth='1.0', linestyle='--')

    plt.subplot(414)
    plt.plot(t, x[:, 3])
    plt.plot(avgt, np.array(lavg[0][3]), color='r')
    plt.plot(avgt, np.array(lavg[1][3]), color='y')
    plt.plot(t, lbv, color='m', linewidth='1.0', linestyle='--')
    plt.xlabel('Time / s')
    plt.ylabel('feq4 v / m/s')

    plt.show()


def vplotOne(lv, lavg=[], title=''):
    t = np.arange(0, len(lv))
    avgt = np.arange(AvgNum, len(lavg)+AvgNum)
    x = np.array(lv)

    basev = 0
    if title is not '':
        basev = PumpVCal(title)
    lbv = np.full((len(lv),1), basev)

    # 绘图
    plt.figure(dpi=100, figsize=(14, 8))
    #plt.subplot(411)
    plt.title(title)
    plt.ylabel('v / m/s')
    plt.plot(t, x)
    plt.plot(avgt, np.array(lavg), color='r')
    #plt.plot(avgt, np.array(lavg), color='g')
    plt.plot(t, lbv, color='m', linewidth='1.0', linestyle='--')


    '''
    x,y:表示坐标值上的值
    string:表示说明文字
    fontsize:表示字体大小
    verticalalignment：垂直对齐方式 ，参数：[ ‘center’ | ‘top’ | ‘bottom’ | ‘baseline’ ]
    horizontalalignment：水平对齐方式 ，参数：[ ‘center’ | ‘right’ | ‘left’ ]
    '''
    pltText = json.dumps(dt)
    plt.text(25, lbv[0][0]+0.2, pltText, fontsize=15)


    plt.show()


def pltbar(title=''):
    dnum = 20
    step = 200000/1024
    start = step*150
    end =  step*(150+dnum)

    fl = fState[:dnum]
    #x = np.arange(0, len(fl))
    x = ["{}".format((i+1)*step) for i in range(150, 150+dnum)]
    y = np.array(fl)
    plt.bar(x, y)

    plt.title(title)
    plt.xticks(rotation=45)
    #plt.xlabel("Feq")
    plt.show()

def claMinMaxFeq():
    if h == 0:
        MinFeq = Fs / Fnum * 163  # 161:31445.3125,   162:31640.625
        MaxFeq = Fs / Fnum * 165  # 164:32031.25      165:32226.5625
    elif h == 1:
        MinFeq = Fs / Fnum * 164  # 161:31445.3125,   162:31640.625
        MaxFeq = Fs / Fnum * 166  # 164:32031.25      165:32226.5625


if __name__ == '__main__':
    Fs = (25e6 / 4) / (2.5 + 12.5 + 7)
    Fnum = 1024
    OffsetFreq = 32e3
    feqStep = Fs / Fnum
    AvgNum = 24 // 2
    StopMinFeq = Fs/Fnum * 163  # 161:31445.3125,   162:31640.625   163:31835.9375
    StopMaxFeq = Fs/Fnum * 165  # 164:32031.25      165:32226.5625

    LowMinFeq = Fs / Fnum * 163  # 161:31445.3125,   162:31640.625
    LowMaxFeq = Fs / Fnum * 166  # 164:32031.25      166:32421.875
    dt = {'lastFeq':33386, 'step':2, 'threshold':8}


    #path = 'data221228-声研所2-180Hz-9.txt'
    rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-30\\'
    dirpath = u'\\2023-6-30-32k-N1-rrs-1Hzfour-4096\\'
    #rootpath = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-8'
    #dirpath = u'\\2023-6-8-32k-5hz\\'
    #fileName = u'2023-6-5-8k-v03.txt'
    #print(fileName)

    '''
    lv = traversalSubdirectory(rootpath + dirpath)
    lavg = [avglv(lv)]
    lavg.append(WeightAvglv(lv))
    print('avg v is:', lavg)
    vplot(lv, lavg, dirpath[1:-1])
    #pltbar(dirpath[1:-1])
    '''

    lf = traversalSubdirectory(rootpath + dirpath)
    # lf 三维数组
    # 元素个数 [[幅值，频率]]


    '''
    1、按频率从小到大排序
    2、连续性判断，补点后，选择频率中间值
    '''
    #lfeq = feqSelect(lf, feqStep)


    '''
    1、top4频率点统计，仅统计过去的24个点
    2、基于统计中间值，+1K，作为右门限值
    3、选择4点中频率最大的
    '''
    lfeq = feqSelectMax(lf, dt, 24)


    # 流速计算
    lv = ListFlowCal(lfeq)

    lavg = avglvOne(lv)
    #lavg.append(WeightAvglv(lv))
    vplotOne(lv, lavg, dirpath[1:-1])
    #pltbar(lf, dirpath[1:-1])
