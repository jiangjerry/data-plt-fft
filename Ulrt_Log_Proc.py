import os

def txtFileWrite(path, name, ld):
    try:
        os.mkdir(path)
        print(path, 'is ok!')
    except Exception  as e:
        #print(e)
        pass
    fname = path + '\\' + name
    f = open(fname, 'w', encoding='utf-8')
    for contents in ld:
        f.write(contents)
    f.close()
    print(fname, 'save ok!')


if __name__ == '__main__':
    ld = []
    startFlag = False
    num = 1

    #path = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-16\\2023-6-15-32k-2#-15hz.txt'
    path = u'D:\\Work\\工作资料\\流量计项目\\测试及数据\\日日顺现场管道测试 2023-6-30\\2023-6-30-32k-N1-rrs-0Hz.txt'
    fileName = os.path.basename(path)
    filePath = os.path.dirname(path)
    print(fileName, filePath)

    count = 0
    f = open(path)
    lines = f.readlines()
    for line in lines:
        if 'ADC1_DMA_Test Reading' in line:
            if startFlag == False:
                startFlag = True
                count += 1
            elif count >= 7:
                count = 0
                # save file 
                newFilePath = filePath + '\\' + fileName[:-4] + 'eigth-8192'
                strnum = '000' + str(num)
                newFileName = fileName[:-4] + strnum[-3:] + '.txt'
                txtFileWrite(newFilePath, newFileName, ld)
                num += 1
                ld = []
            else:
                count += 1
        if startFlag:    
            ld += [line]
